////////////////////
// How to install //
////////////////////
1) Copy git project (download and copy) into cfusion folder.
For example, git project has a "wwwroot" folder, acting like "public"
interface, and all other system files stored in one directory above.
You need find in your coldfusion installation similar directory, as "cfusion" 
(in my case), which has another "wwwroot" folder as webroot, and you must copy
git project into this ("cfusion") folder.
2) Then, you need add a few mappings in admin panel and one datasource:
- add /cfc mapping, which points at "/cfc" directory in project root
- add /templates mapping pointing at "/templates" in project root
- add postgresql datasource, name it "cf_bugtracker", and set credentials
accordingly to your database connection for this project.
3) Find sql dump (stored in "/resources" folder) and fill your database.
4) Sql-dump contains one "account" record, so after this steps you can try
login with credentials -
"email@com"
"admin123"
5) If you done all right, you must see empty table.
After doing that you can try create a new issue and check that all works
