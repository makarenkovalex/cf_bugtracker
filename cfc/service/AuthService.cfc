component {
    public void function auth(String email, String password) {
        account = entityLoad("Account", {email: email}, true);
        if (not isNull(account)) {
            inputHash = Hash(password & account.salt, "SHA-512");
            if (inputHash == account.password) {
                session.auth.isLoggedIn = true;
                session.auth.account_id = account.id;
                session.auth.email = account.email;
                session.auth.name = account.name;
                session.auth.surname = account.surname;
            }
        }
    }
}