component persistent="true" table="issue" {
    property name="id" fieldtype="id" generator="increment";
    property name="created_at" ormtype="timestamp";
    property name="name" ormtype="string";
    property name="description" ormtype="text";
    property name="status" ormtype="string";
    property name="urgency" ormtype="string";
    property name="criticality" ormtype="string";
    // relations
    property name="account" fieldtype="many-to-one" cfc="Account";
    property name="logs" fieldtype="one-to-many" cfc="IssueLog" fkcolumn="issue_id";

    public Array function getAvailableStatuses() {
        if (this.status == "New") {
            return [
                "New",
                "Open",
                "Solved",
                "Closed"
            ];
        } else if (this.status == "Open") {
            return [
                "Open",
                "Solved",
                "Closed"
            ];
        } else if (this.status == "Solved") {
            return [
                "Open",
                "Solved",
                "Closed"
            ];
        } else if (this.status == "Closed") {
            return [
                "Closed"
            ];
        } else {
            return [
                "New",
                "Open",
                "Solved",
                "Closed"
            ];
        }
    }

    public Array function getStatusForNew() {
        return [
            "New"
        ];
    }

    public Array function getAllUrgencies() {
        return [
            "Very urgently",
            "Urgently",
            "Not urgently",
            "Absolutely do not rush"
        ];
    }

    public Array function getAllCriticalities() {
        return [
            "Accident",
            "Critical",
            "Uncritical",
            "Request for change"
        ];
    }
}   