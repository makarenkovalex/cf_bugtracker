component persistent="true" table="account" {
    property name="id" column="id" fieldtype="id" generator="increment";
    property name="email" ormtype="string";
    property name="name" ormtype="string";
    property name="surname" ormtype="string";
    property name="password" ormtype="string";
    property name="salt" ormtype="string";
    // relations
    property name="issue" fieldtype="one-to-many" cfc="Issue" fkcolumn="account_id";
    property name="issueLog" fieldtype="one-to-many" cfc="IssueLog" fkcolumn="account_id";
}
