--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account; Type: TABLE; Schema: public; Owner: cf_bugtracker
--

CREATE TABLE public.account (
    email character varying NOT NULL,
    name character varying NOT NULL,
    surname character varying NOT NULL,
    password character varying NOT NULL,
    id integer NOT NULL,
    salt character varying NOT NULL
);


ALTER TABLE public.account OWNER TO cf_bugtracker;

--
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: cf_bugtracker
--

CREATE SEQUENCE public.account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO cf_bugtracker;

--
-- Name: account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cf_bugtracker
--

ALTER SEQUENCE public.account_id_seq OWNED BY public.account.id;


--
-- Name: issue; Type: TABLE; Schema: public; Owner: cf_bugtracker
--

CREATE TABLE public.issue (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    name character varying NOT NULL,
    description text,
    account_id integer NOT NULL,
    status character varying NOT NULL,
    urgency character varying NOT NULL,
    criticality character varying NOT NULL
);


ALTER TABLE public.issue OWNER TO cf_bugtracker;

--
-- Name: issue_id_seq; Type: SEQUENCE; Schema: public; Owner: cf_bugtracker
--

CREATE SEQUENCE public.issue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_id_seq OWNER TO cf_bugtracker;

--
-- Name: issue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cf_bugtracker
--

ALTER SEQUENCE public.issue_id_seq OWNED BY public.issue.id;


--
-- Name: issue_log; Type: TABLE; Schema: public; Owner: cf_bugtracker
--

CREATE TABLE public.issue_log (
    id integer NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    action character varying NOT NULL,
    comment text,
    account_id integer NOT NULL,
    issue_id integer NOT NULL
);


ALTER TABLE public.issue_log OWNER TO cf_bugtracker;

--
-- Name: issue_log_id_seq; Type: SEQUENCE; Schema: public; Owner: cf_bugtracker
--

CREATE SEQUENCE public.issue_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_log_id_seq OWNER TO cf_bugtracker;

--
-- Name: issue_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cf_bugtracker
--

ALTER SEQUENCE public.issue_log_id_seq OWNED BY public.issue_log.id;


--
-- Name: account id; Type: DEFAULT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.account ALTER COLUMN id SET DEFAULT nextval('public.account_id_seq'::regclass);


--
-- Name: issue id; Type: DEFAULT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue ALTER COLUMN id SET DEFAULT nextval('public.issue_id_seq'::regclass);


--
-- Name: issue_log id; Type: DEFAULT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue_log ALTER COLUMN id SET DEFAULT nextval('public.issue_log_id_seq'::regclass);


--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: cf_bugtracker
--

COPY public.account (email, name, surname, password, id, salt) FROM stdin;
email@com	aleks	makarenkov	663865868D17DA86301BB1AFE32CF17CB32936F5364EE346AA2736E5AEC9E5951884E24B497FCC111E4EAE72BBE4D6F258C10D18156AF040C18B1F844056CE3A	2	2FFD95A818A3A98D3ADFCD50D7BFA227FC6F51CE8ABFD4D1F488D29EB877CFD928258E94BCCA327D9B210B094C7A73EDCCB2E9B1ABBDFAB939BBE5649B06AAD7
\.


--
-- Data for Name: issue; Type: TABLE DATA; Schema: public; Owner: cf_bugtracker
--

COPY public.issue (id, created_at, name, description, account_id, status, urgency, criticality) FROM stdin;
\.


--
-- Data for Name: issue_log; Type: TABLE DATA; Schema: public; Owner: cf_bugtracker
--

COPY public.issue_log (id, updated_at, action, comment, account_id, issue_id) FROM stdin;
\.


--
-- Name: account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cf_bugtracker
--

SELECT pg_catalog.setval('public.account_id_seq', 2, true);


--
-- Name: issue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cf_bugtracker
--

SELECT pg_catalog.setval('public.issue_id_seq', 3, true);


--
-- Name: issue_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cf_bugtracker
--

SELECT pg_catalog.setval('public.issue_log_id_seq', 2, true);


--
-- Name: account account_pk; Type: CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pk PRIMARY KEY (id);


--
-- Name: issue_log issue_log_pk; Type: CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue_log
    ADD CONSTRAINT issue_log_pk PRIMARY KEY (id);


--
-- Name: issue issue_pk; Type: CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT issue_pk PRIMARY KEY (id);


--
-- Name: account user_un; Type: CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT user_un UNIQUE (email);


--
-- Name: issue_account_id_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_account_id_idx ON public.issue USING btree (account_id);


--
-- Name: issue_created_at_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_created_at_idx ON public.issue USING btree (created_at);


--
-- Name: issue_criticality_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_criticality_idx ON public.issue USING btree (criticality);


--
-- Name: issue_log_account_id_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_log_account_id_idx ON public.issue_log USING btree (account_id);


--
-- Name: issue_log_action_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_log_action_idx ON public.issue_log USING btree (action);


--
-- Name: issue_log_issue_id_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_log_issue_id_idx ON public.issue_log USING btree (issue_id);


--
-- Name: issue_log_updated_at_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_log_updated_at_idx ON public.issue_log USING btree (updated_at);


--
-- Name: issue_name_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_name_idx ON public.issue USING btree (name);


--
-- Name: issue_status_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_status_idx ON public.issue USING btree (status);


--
-- Name: issue_urgency_idx; Type: INDEX; Schema: public; Owner: cf_bugtracker
--

CREATE INDEX issue_urgency_idx ON public.issue USING btree (urgency);


--
-- Name: issue issue_account_fk; Type: FK CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT issue_account_fk FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: issue_log issue_log_account_fk; Type: FK CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue_log
    ADD CONSTRAINT issue_log_account_fk FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: issue_log issue_log_issue_fk; Type: FK CONSTRAINT; Schema: public; Owner: cf_bugtracker
--

ALTER TABLE ONLY public.issue_log
    ADD CONSTRAINT issue_log_issue_fk FOREIGN KEY (issue_id) REFERENCES public.issue(id);


--
-- PostgreSQL database dump complete
--

