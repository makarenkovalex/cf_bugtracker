<div id="login-form-container">
    <form class="form" action="/rpc/authenticate.cfm" method="post">
        <div class="form-header">Sign in</div>
        <input class="input" type="text" name="email" placeholder="E-mail">
        <input class="input" type="password" name="password" placeholder="Password">
        <input class="input-button" type="submit" value="Enter">
    </form>
</div>
