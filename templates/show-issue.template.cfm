<div id="issue-container">
    <cfoutput>
        <form
            id="save-issue-form"
            class="save-form"
            action="/rpc/save-issue.cfm"
            method="post"
            onsubmit="return validateIssueForm(this)"
        >
            <cfif not isDefined("url.id")>
                <div class="form-header"> New issue </div>
            <cfelse>
                <div class="form-header"> Edit issue </div>
            </cfif>
            <input type="text" name="id" value="#issue.id#" hidden>
            <input
                class="save-input"
                type="text"
                name="name"
                value="#issue.name#"
                placeholder="Name"
            >
            <textarea
                class="save-input"
                name="description"
                form="save-issue-form"
                rows="5"
                placeholder="Description"
            >#issue.description#</textarea>
            <select
                id="save-issue-status"
                class="save-input"
                name="status"
                onchange="statusChanged(this)"
            >
                <cfloop array="#issue.statuses#" index="status">
                    <cfif status == issue.status>
                        <cfset selected = "selected">
                    <cfelse>
                        <cfset selected = "">
                    </cfif>
                    <option #selected# value="#status#">#status#</option>
                </cfloop>
            </select>
            <select class="save-input" name="urgency">
                <cfloop array="#issue.urgencies#" index="urgency">
                    <cfif urgency == issue.urgency>
                        <cfset selected = "selected">
                    <cfelse>
                        <cfset selected = "">
                    </cfif>
                    <option #selected# value="#urgency#">#urgency#</option>
                </cfloop>
            </select>
            <select class="save-input" name="criticality">
                <cfloop array="#issue.criticalities#" index="criticality">
                    <cfif criticality == issue.criticality>
                        <cfset selected = "selected">
                    <cfelse>
                        <cfset selected = "">
                    </cfif>
                    <option #selected# value="#criticality#">#criticality#</option>
                </cfloop>
            </select>
            <textarea
                id="save-issue-comment"
                class="save-input"
                name="comment"
                form="save-issue-form"
                placeholder="Comment"
                rows="2"
            ></textarea>
            <input class="input-button" type="submit" value="Save">
            <a class="input-button" href="/rpc/show-issues.cfm">Back</a>
            <cfif issue.hasLogs()>
                <cfset logs = issue.getLogs()>
                <div class="table">
                    <div class="table-header-row">
                        <div class="table-header-cell table-cell-3">Action</div>
                        <div class="table-header-cell table-cell-3">Comment</div>
                        <div class="table-header-cell table-cell-3">Updated at</div>
                    </div>
                    <cfloop array="#logs#" index="log">
                        <div class="table-row">
                            <div class="table-cell table-cell-3">
                                #log.action#
                            </div>
                            <div class="table-cell table-cell-3">
                                #log.comment#
                            </div>
                            <div class="table-cell table-cell-3">
                                #log.updated_at#
                            </div>
                        </div>

                    </cfloop>
                </div>
            </cfif>
        </form>
    </cfoutput>
</div>
