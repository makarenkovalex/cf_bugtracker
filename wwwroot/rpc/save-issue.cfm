<cfif not IsDefined("session.auth.isLoggedIn")>
    <cflocation url="/" addtoken="false">
<cfelse>
    <cfset issuesService = new cfc.service.IssueService()>
    <cfset id = (isDefined("form.id") and form.id != "" ? form.id : 0)>
    <cfset name = (isDefined("form.name") ? form.name : "")>
    <cfset description = (isDefined("form.description") ? form.description : "")>
    <cfset status = (isDefined("form.status") ? form.status : "")>
    <cfset urgency = (isDefined("form.urgency") ? form.urgency : "")>
    <cfset criticality = (isDefined("form.criticality") ? form.criticality : "")>
    <cfset comment = (isDefined("form.comment") ? form.comment : "")>

    <cfset issue = issuesService.saveIssue(
        id,
        name,
        description,
        status,
        urgency,
        criticality,
        comment,
        session.auth.account_id
    )>
    <cfset link = "/rpc/show-issue.cfm?id=" & issue.id>
    <cflocation url="#link#" addtoken="false">
</cfif>
