<cfif not IsDefined("session.auth.isLoggedIn")>
    <cflocation url="/" addtoken="false">
<cfelse>
    <cfset issuesService = new cfc.service.IssueService()>
    <cfset pageData = issuesService.showIssues()>
    <cfinclude template="/templates/header.template.cfm"/>
    <cfinclude template="/templates/show-issues.template.cfm"/>
    <cfinclude template="/templates/footer.template.cfm"/>
</cfif>
